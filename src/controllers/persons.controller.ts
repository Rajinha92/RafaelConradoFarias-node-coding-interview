import {
    JsonController,
    Get,
    Post,
    Body,
    Delete,
    Param,
} from 'routing-controllers'
import { PersonsService } from '../services/persons.service'

const personsService = new PersonsService()

@JsonController('/persons', { transformResponse: false })
export class PersonsController {
    @Get()
    async getAll() {
        const data = await personsService.getAll()
        return {
            status: 200,
            data,
        }
    }

    @Post('')
    async create(@Body() p: any) {
        const data = await personsService.create(p)
        return {
            status: 201,
            data,
        }
    }

    @Delete('/:personId')
    async remove(@Param('personId') personId: string) {
        const data = await personsService.remove(personId)
        if (!data)
            return {
                success: false,
                error: 'Person not found',
            }
        return {
            status: 200,
            data,
        }
    }
}
