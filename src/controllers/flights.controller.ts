import { JsonController, Get, Put, Param } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        const data = await flightsService.getAll()
        return {
            status: 200,
            data,
        }
    }

    @Put('/:flightId/onboard/:personId')
    async onBoard(
        @Param('flightId') flightId: string,
        @Param('personId') personId: string
    ) {
        try {
            const data = await flightsService.onBoard(flightId, personId)
            if (!data)
                return {
                    success: false,
                    error: 'Flight not found',
                }
            return {
                success: true,
                data,
            }
        } catch (e: any) {
            return {
                success: false,
                error: e.message,
            }
        }
    }
}
