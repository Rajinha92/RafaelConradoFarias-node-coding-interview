import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    getAll() {
        return FlightsModel.find()
    }

    async onBoard(flightId: string, personId: string) {
        const flight = await FlightsModel.findById(flightId)
        if (!flight) return null
        flight.passengers = Array.from(
            new Set([...flight.passengers, personId])
        )
        await flight.save()
        return flight
    }
}
