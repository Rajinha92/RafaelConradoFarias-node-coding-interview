import { PersonsModel } from '../models/persons.model'

export class PersonsService {
    getAll() {
        return PersonsModel.find({
            deletedAt: null,
        })
    }

    create(person: any) {
        return PersonsModel.create(person)
    }

    remove(personId: string) {
        return PersonsModel.findByIdAndUpdate(personId, {
            $set: {
                deletedAt: new Date(),
            },
        })
    }
}
