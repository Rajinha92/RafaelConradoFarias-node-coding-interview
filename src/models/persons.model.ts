import { IsEmail, IsEnum, Length } from 'class-validator'
import mongoose, { Schema } from 'mongoose'

enum Gender {
    Male = 'Male',
    Female = 'Female',
}

class Person {
    @Length(2, 255)
    name: string

    @IsEmail()
    email: string

    @IsEnum(Gender)
    gender: string

    type: string
    deletedAt?: Date
}

const schema = new Schema<Person>(
    {
        name: { required: true, type: String },
        email: { required: true, type: String },
        gender: {
            required: true,
            type: String,
            enum: {
                values: ['Male', 'Female'],
                message: '{VALUE} gender is not supported',
            },
        },
        type: { type: String, required: true },
        deletedAt: { type: Date },
    },
    { timestamps: true }
)

export const PersonsModel = mongoose.model('Persons', schema)
